
#ifndef LOGICAL_UNIFICATION_HH
#define LOGICAL_UNIFICATION_HH


#include "notation.hh"
#include "collections.hh"


namespace Logical
{


class UnificationError
{

};


static inline Substitution unifier(const Notation& first, const Notation& second)
{
	if(first.get_symbol() != second.get_symbol())
		throw UnificationError();
	
	if(first.size() != second.size())
		throw UnificationError();
	
	if(first.is_variable() && second.is_expression())
	{
		if(second.free_variables().count((const VariableExpression&)first))
			throw UnificationError();
		auto substitution = Substitution();
		substitution[(const VariableExpression&)first] = make_expression_pointer((const Expression&)second);
		return substitution;
	}
	
	if(second.is_variable() && first.is_expression())
	{
		if(first.free_variables().count((const VariableExpression&)second))
			throw UnificationError();
		auto substitution = Substitution();
		substitution[(const VariableExpression&)second] = make_expression_pointer((const Expression&)first);
		return substitution;
	}
	
	return (Shadow(first) % Shadow(second))
		.parallel_map([](const auto& p) -> Substitution
		{
			return unifier(p.first, p.second);
		})
		.reduce([](const Substitution& subst_a, const Substitution& subst_b)
		{
			Substitution subst_r;
			VariableSet a_keys, common_keys;
			
			for(const auto& item : subst_a)
			{
				const auto& key = item.first;
				a_keys.insert(key);
				subst_r[key] = make_expression_pointer(*item.second);
			}
			
			for(const auto& item : subst_b)
			{
				const auto& key = item.first;
				if(a_keys.count(key))
					common_keys.insert(key);
				subst_r[key] = make_expression_pointer(*item.second);
			}
			
			for(const auto& key : common_keys)
			{
				const auto u = unifier(*subst_a.at(key), *subst_b.at(key));
				const auto a = subst_a.at(key)->substitute(u);
				const auto b = subst_b.at(key)->substitute(u);
				logical_assert(a == b);
				subst_r[key] = make_expression_pointer(a);
			}
			
			return subst_r;
		});
}


}; // namespace Logical


#endif // LOGICAL_UNIFICATION_HH

