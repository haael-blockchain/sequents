
#ifndef LOGICAL_LAMBDA_HH
#define LOGICAL_LAMBDA_HH


#include "expression.hh"
#include "formula.hh"


namespace Logical
{


class FormulaWrapper : public Expression
{
private:
	Formula formula;

protected:
	static const auto tag = type_hash("FormulaWrapper");

public:
	template<typename FormulaT>
	FormulaWrapper(FormulaT&& f)
	: formula(forward<FormulaT>(f))
	{
	}
	
	virtual ExpressionReference operator[](size_t index) const
	{
		if(formula.is_atomic())
			return (const Expression&)(formula[index]);
		else
			return FormulaWrapper((const Formula&)(formula[index]));
	}
	
	virtual size_t size(void) const
	{
		return formula.size();
	}
	
	virtual uint64_t hash(uint64_t seed = 0) const
	{
		return formula.hash(seed + 267109) + 7241092 + seed >> 4;
	}
	
	virtual bool identical(const Expression& other) const
	{
		if(get_tag() != other.get_tag())
			return false;
		
		return formula == static_cast<const FormulaWrapper&>(other).formula;
	}


};







class Lambda : public Expression
{
public:
	enum class OperatorType : uint8_t
	{
		ABSTRACTION, APPLICATION, CONDITION, LOOP, VOID, RECURSE
	};

private:
	OperatorType operator_type;
};


class Abstraction : public Lambda
{
private:
	string name;
	VariableSet arguments;
	Formula in_spec, out_spec;
	unique_ptr<Expression> expression;
};


class Application : public Lambda
{
private:
	unique_ptr<Expression> expression;
	Substitution arguments;
};


class Condition : public Lambda
{
private:
	Formula condition;
	unique_ptr<Expression> yes_clause, no_clause;
};


class Loop : public Lambda
{
private:
	unique_ptr<Expression> condition, kernel;
	Formula invariant;
};


class Void : public Lambda
{
private:
	string message;
};


class Recurse : public Lambda
{
private:
	string name;
};


}; // namespace Logical


#endif // LOGICAL_LAMBDA_HH

