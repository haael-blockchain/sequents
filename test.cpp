

#define DEBUG


#include <atomic>
#include <csignal>
#include <cstdlib>
#include <exception>
#include <iostream>
#include <thread>
//#include <pthread.h>
#include <sys/syscall.h>
#include <unistd.h>


#ifdef DEBUG
namespace execinfo
{
#include <execinfo.h>
};
#endif


using std::cerr;
using std::cout;
using std::endl;
using std::set_terminate;
using std::exception;


#include "collections.hh"
#include "errors.hh"
#include "notation.hh"
#include "sequent.hh"
#include "sync.hh"
#include "unionfind.hh"


using namespace Logical;

volatile atomic_size_t Logical::max_thread_count(std::thread::hardware_concurrency() * 2);
volatile sig_atomic_t Logical::thread_error(false);


void signal_received(int sig_num)
{
	Logical::thread_error = true;
}


#ifdef DEBUG
static constexpr size_t segfault_stack_max = 256;
void* segfault_stack[segfault_stack_max];
#endif

void segfault_received(int sig_num)
{
	Logical::thread_error = true;

#ifdef DEBUG
	size_t stack_size = execinfo::backtrace(segfault_stack, segfault_stack_max);
	char** stack_trace = execinfo::backtrace_symbols(segfault_stack, stack_size);
	for(size_t i = 0; i < stack_size; i++)
		cout << " " << stack_trace[i] << endl;
#endif

	abort();
}


int main(int argc, char* argv[])
{
	signal(SIGTERM, signal_received);
	signal(SIGABRT, signal_received);
	signal(SIGSEGV, segfault_received);
	
	try
	{
		cout << "max thread count (0 = unlimited): " << max_thread_count << endl;
		cout << "sync_test" << endl;
		sync_test();
		
		cout << "collections_test" << endl;
		collections_test();
		
		cout << "unionfind_test" << endl;
		unionfind_test();
		
		cout << "expression_test" << endl;
		expression_test();
		
		cout << "formula_test" << endl;
		notation_test();
		
		cout << "sequent_test" << endl;
		sequent_test();
	}
	catch(const AssertionError& error)
	{
		cout << error.file << ":" << error.line << ": AssertionError " << error.message << endl;
//ifdef DEBUG
		// char** stack_trace = execinfo::backtrace_symbols(error.stack, error.stack_size);
		// for(size_t i = 0; i < error.stack_size; i++)
		//	cout << " " << stack_trace[i] << endl;
//endif
	}
	catch(const Error& error)
	{
		cout << "Error " << error.message << endl;
#ifdef DEBUG
		char** stack_trace = execinfo::backtrace_symbols(error.stack, error.stack_size);
		for(size_t i = 0; i < error.stack_size; i++)
			cout << " " << stack_trace[i] << endl;
#endif
	}
	catch(const exception& error)
	{
		cout << "System exception: " << error.what() << endl;
	}
	catch(...)
	{
		cout << "Unknown exception" << endl;
	}

	return 0;
}
