
#ifndef LOGICAL_NOTATION_HH
#define LOGICAL_NOTATION_HH


#include <cstddef>
#include <string_view>
#include <tuple>
#include <initializer_list>
#include <unordered_set>
#include <unordered_map>
#include <ostream>
#include <type_traits>

#include "logical.hh"
#include "errors.hh"
#include "collections.hh"


namespace Logical
{

using std::size_t;
using std::string_view;
using std::tuple;
using std::make_tuple;
using std::tuple_size;
using std::initializer_list;
using std::forward;
using std::unordered_set;
using std::unordered_map;
using std::ostream;
using std::enable_if_t;
using std::is_base_of_v;


struct VariableHash
{
	uint64_t operator () (const VariableExpression& v) const;
};

struct VariableCompare
{
	bool operator () (const VariableExpression& u, const VariableExpression& v) const;
};

typedef const Expression* ExpressionPointer;

typedef unordered_set<VariableExpression, VariableHash, VariableCompare> VariableSet;

typedef unordered_map<VariableExpression, ExpressionPointer, VariableHash, VariableCompare> Substitution;


extern const VariableSet empty_variable_set;


#ifdef __builtin_parityll

static inline constexpr bool parity(uint64_t x)
{
	return __builtin_parityll(x);
}

#else

static inline constexpr bool parity(uint64_t x)
{
	for(uint8_t i = 0; i <= 5; i++)
		x ^= x >> (1 << (5 - i));
	return (bool)(x & 1);
}

#endif


static constexpr uint64_t hash_step(uint8_t ch, uint64_t seed, uint64_t polynomial)
{
	return ((seed << 1) | (uint64_t)parity(seed & polynomial)) + ((uint64_t)ch << ((seed >> 10) % 48));
}


class Symbol
{
private:
	string_view value;
	bool infix;

public:
	constexpr Symbol(const string_view& sv, bool i)
	: value(sv), infix(i)
	{
	}
	
	const string_view& get_value(void) const
	{
		return value;
	}
	
	const bool get_infix(void) const
	{
		return infix;
	}
	
	bool operator == (const Symbol& other) const
	{
		return value == other.value && infix == other.infix;
	}
	
	bool operator != (const Symbol& other) const
	{
		return !(value == other.value);
	}
	
	constexpr uint64_t hash(uint64_t seed = 0xa759af0c87a2109bULL) const
	{
		for(const auto ch : value)
			seed = hash_step(ch, seed, 0xc000000000000001ULL);
		seed ^= (uint64_t)infix;
		return seed;
	}
	
	constexpr operator uint64_t (void) const
	{
		return hash();
	}
};


struct SymbolHash
{
	uint64_t operator () (const Symbol& s) const
	{
		return s.hash();
	}
};


ostream& operator << (ostream& out, const Symbol& symbol)
{
	out << symbol.get_value();
	return out;
}


class ConnectiveSymbol : public Symbol
{
public:
	constexpr ConnectiveSymbol(const string_view& sv, bool i)
	: Symbol(sv, i)
	{
	}
	
	template<typename... Formulas>
	constexpr auto operator () (Formulas&&... f) const;
};


class RelationSymbol : public Symbol
{
public:
	constexpr RelationSymbol(const string_view& sv, bool i)
	: Symbol(sv, i)
	{
	}
	
	template<typename... Expressions>
	constexpr auto operator () (Expressions&&... e) const;
};


class FunctionSymbol : public Symbol
{
public:
	constexpr FunctionSymbol(const string_view& sv, bool i)
	: Symbol(sv, i)
	{
	}
	
	template<typename... Expressions>
	constexpr auto operator () (Expressions&&... e) const;
};


class VariableSymbol : public Symbol
{
public:
	constexpr VariableSymbol(const string_view& sv, bool i)
	: Symbol(sv, i)
	{
	}
	
	constexpr auto operator () (void) const;
};


class Notation
{
public:
	typedef Notation value_type;
	
	virtual const Symbol& get_symbol(void) const = 0;
	virtual size_t size(void) const = 0;
	virtual const Notation& operator [] (size_t index) const = 0;
	virtual const VariableSet& capture_variables(void) const = 0;
	
	uint64_t hash(uint64_t seed = 0x1a739812a08351feULL) const;
	
	Iterator<Notation> begin(void) const
	{
		return Iterator<Notation>(*this, 0);
	}
	
	Iterator<Notation> end(void) const
	{
		return Iterator<Notation>(*this, size());
	}
	
	operator bool (void) const;
	
	virtual bool is_formula(void) const { return false; }
	virtual bool is_compound(void) const { return false; }
	virtual bool is_quantifier(void) const { return false; }
	virtual bool is_atomic(void) const { return false; }
	virtual bool is_expression(void) const { return false; }
	virtual bool is_constant(void) const { return false; }
	virtual bool is_capture(void) const { return false; }
	virtual bool is_variable(void) const { return false; }
	
	virtual VariableSet free_variables(void) const;
	
	size_t total_size(void) const;
};

	
ostream& operator << (ostream& out, const Notation& notation)
{
	const auto& symbol = notation.get_symbol();
	
	if(symbol.get_infix())
	{
		if(notation.size() <= 1)
			out << symbol;
		
		bool first = true;
		for(const auto& child : notation)
		{
			if(!first)
				out << " " << symbol << " ";
			else
				first = false;
			
			if(child.get_symbol().get_infix() && child.size() > 0)
				out << "(" << child << ")";
			else
				out << child;
		}
	}
	else
	{
		out << notation.get_symbol();
		out << "(";
		
		bool first = true;
		for(const auto& child : notation)
		{
			if(!first)
				out << ", ";
			else
				first = false;
			
			if(child.get_symbol().get_infix() && child.size() > 1 && notation.size() > 1)
				out << "(" << child << ")";
			else
				out << child;
		}
		
		out << ")";
	}
	
	return out;
}


class Formula : public Notation
{
public:
	typedef Notation value_type;
	
	bool is_formula(void) const { return true; }
};


class GeneralCompoundFormula : public Formula
{
public:
	typedef Formula value_type;
	
	virtual const ConnectiveSymbol& get_symbol(void) const = 0;
	virtual const Formula& operator [] (size_t index) const = 0;
	
	bool is_compound(void) const { return true; }
};


template<typename Collection>
class CompoundFormula : public GeneralCompoundFormula
{
private:
	ConnectiveSymbol symbol;
	Collection formula;

public:
	typedef Formula value_type;
	
	constexpr CompoundFormula(const ConnectiveSymbol& s, Collection&& f)
	: symbol(s), formula(forward<Collection>(f))
	{
	}
	
	const ConnectiveSymbol& get_symbol(void) const
	{
		return symbol;
	}
	
	size_t size(void) const
	{
		return formula.size();
	}
	
	const Formula& operator [] (size_t index) const
	{
		return formula[index];
	}
	
	const VariableSet& capture_variables(void) const
	{
		return empty_variable_set;
	}
};


static inline const Formula& default_get_formula_0(const tuple<>& t)
{
	throw RuntimeError("Empty tuple does not have element 0.");
}

static inline const Formula& default_get_formula_0(const tuple<const Formula&>& t)
{
	return std::get<0>(t);
}

static inline const Formula& default_get_formula_0(const tuple<const Formula&, const Formula&>& t)
{
	return std::get<0>(t);
}

static inline const Formula& default_get_formula_1(const tuple<>& t)
{
	throw RuntimeError("Empty tuple does not have element 1.");
}

static inline const Formula& default_get_formula_1(const tuple<const Formula&>& t)
{
	throw RuntimeError("Single-element tuple does not have element 1.");
}

static inline const Formula& default_get_formula_1(const tuple<const Formula&, const Formula&>& t)
{
	return std::get<1>(t);
}


template<typename... Formulas>
class CompoundFormula<tuple<Formulas...>> : public GeneralCompoundFormula
{
private:
	ConnectiveSymbol symbol;
	using Collection = tuple<Formulas...>;
	Collection formula;

public:
	typedef Formula value_type;
	
	constexpr CompoundFormula(const ConnectiveSymbol& s, Collection&& f)
	: symbol(s), formula(forward<Collection>(f))
	{
	}
	
	const ConnectiveSymbol& get_symbol(void) const
	{
		return symbol;
	}
	
	size_t size(void) const
	{
		return tuple_size<Collection>::value;
	}
	
	const Formula& operator [] (size_t index) const
	{
		if(index >= size())
			throw IndexError("Index out of range in tuple-based CompoundFormula.", index, size(), this);
		
		if(index == 0)
			return default_get_formula_0(formula);
		else if(index == 1)
			return default_get_formula_1(formula);
		else
			throw RuntimeError("Unsupported tuple size in tuple-based CompoundFormula.");
	}
	
	const VariableSet& capture_variables(void) const
	{
		return empty_variable_set;
	}
};


/*

template<typename Collection>
class BindingFormula : public Formula
{
private:
	QuantifierSymbol symbol;
	//VariableSet variable;
	Collection formula;
};

*/


class Expression : public Notation
{
public:
	typedef Expression value_type;
	
	bool is_expression(void) const
	{
		return true;
	}
	
	SubstitutedExpression substitute(const Substitution& substitution) const;
};



class GeneralAtomicFormula : public Formula
{
public:
	typedef Expression value_type;
	
	virtual const RelationSymbol& get_symbol(void) const = 0;
	virtual const Expression& operator [] (size_t index) const = 0;
	
	bool is_atomic(void) const { return true; }
};


template<typename Collection>
class AtomicFormula : public GeneralAtomicFormula
{
private:
	RelationSymbol symbol;
	Collection expression;

public:
	typedef Expression value_type;
	
	constexpr AtomicFormula(const RelationSymbol& s, Collection&& e)
	: symbol(s), expression(forward<Collection>(e))
	{
	}
	
	const RelationSymbol& get_symbol(void) const
	{
		return symbol;
	}
	
	size_t size(void) const
	{
		return expression.size();
	}
	
	const Expression& operator [] (size_t index) const
	{
		return expression[index];
	}
	
	const VariableSet& capture_variables(void) const
	{
		return empty_variable_set;
	}
	
	bool is_atomic(void) const { return true; }
};


static inline const Expression& default_get_expression_0(const tuple<>& t)
{
	throw RuntimeError("");
}

static inline const Expression& default_get_expression_0(const tuple<const Expression&>& t)
{
	return std::get<0>(t);
}

static inline const Expression& default_get_expression_0(const tuple<const Expression&, const Expression&>& t)
{
	return std::get<0>(t);
}

static inline const Expression& default_get_expression_1(const tuple<>& t)
{
	throw RuntimeError("");
}

static inline const Expression& default_get_expression_1(const tuple<const Expression&>& t)
{
	throw RuntimeError("");
}

static inline const Expression& default_get_expression_1(const tuple<const Expression&, const Expression&>& t)
{
	return std::get<1>(t);
}


template<typename... Expressions>
class AtomicFormula<tuple<Expressions...>> : public GeneralAtomicFormula
{
private:
	RelationSymbol symbol;
	using Collection = tuple<Expressions...>;
	Collection expression;

public:
	typedef Expression value_type;
	
	constexpr AtomicFormula(const RelationSymbol& s, Collection&& e)
	: symbol(s), expression(forward<Collection>(e))
	{
	}
	
	const RelationSymbol& get_symbol(void) const
	{
		return symbol;
	}
	
	size_t size(void) const
	{
		return tuple_size<Collection>::value;
	}
	
	const Expression& operator [] (size_t index) const
	{
		if(index >= size())
			throw IndexError("Index out of range in tuple-based AtomicFormula.", index, size(), this);
		
		if(index == 0)
			return default_get_expression_0(expression);
		else if(index == 1)
			return default_get_expression_1(expression);
		else
			throw RuntimeError("Unsupported tuple size in tuple-based AtomicFormula.");
	}
	
	const VariableSet& capture_variables(void) const
	{
		return empty_variable_set;
	}
	
	bool is_atomic(void) const { return true; }
};


template<typename Collection>
class ConstantExpression : public Expression
{
private:
	FunctionSymbol symbol;
	Collection expression;

public:
	typedef Expression value_type;
	
	constexpr ConstantExpression(const FunctionSymbol& s, Collection&& e)
	: symbol(s), expression(forward<Collection>(e))
	{
	}
	
	const FunctionSymbol& get_symbol(void) const
	{
		return symbol;
	}
	
	size_t size(void) const
	{
		return expression.size();
	}
	
	const Expression& operator [] (size_t index) const
	{
		return expression[index];
	}
	
	const VariableSet& capture_variables(void) const
	{
		return empty_variable_set;
	}
	
	bool is_constant(void) const { return true; }
};


template<typename... Expressions>
class ConstantExpression<tuple<Expressions...>> : public Expression
{
private:
	FunctionSymbol symbol;
	using Collection = tuple<Expressions...>;
	Collection expression;

public:
	typedef Expression value_type;
	
	constexpr ConstantExpression(const FunctionSymbol& s, Collection&& e)
	: symbol(s), expression(forward<Collection>(e))
	{
	}
	
	const FunctionSymbol& get_symbol(void) const
	{
		return symbol;
	}
	
	size_t size(void) const
	{
		return tuple_size<Collection>::value;
	}
	
	const Expression& operator [] (size_t index) const
	{
		if(index >= size())
			throw IndexError("Index out of range in tuple-based ConstantExpression.", index, size(), this);
		
		if(index == 0)
			return default_get_expression_0(expression);
		else if(index == 1)
			return default_get_expression_1(expression);
		else
			throw RuntimeError("Unsupported tuple size in tuple-based ConstantExpression.");
	}
	
	const VariableSet& capture_variables(void) const
	{
		return empty_variable_set;
	}
	
	bool is_constant(void) const { return true; }
};


/*

template<typename Collection>
class CaptureExpression : public Expression
{
private:
	CaptureSymbol symbol;
	//VariableSet variable;
	Collection formula;
};

*/


class VariableExpression : public Expression
{
private:
	VariableSymbol symbol;

public:
	typedef Expression value_type;
	
	constexpr VariableExpression(const VariableSymbol& s)
	: symbol(s)
	{
	}
	
	const VariableSymbol& get_symbol(void) const
	{
		return symbol;
	}
	
	size_t size(void) const
	{
		return 0;
	}
	
	const Expression& operator [] (size_t index) const
	{
		throw IndexError("Variables don't have children.", index, size(), this);
	}
	
	const VariableSet& capture_variables(void) const
	{
		return empty_variable_set;
	}
	
	bool is_variable(void) const
	{
		return true;
	}
	
	VariableSet free_variables(void) const;
};



class SubstitutedExpression : public Expression
{
private:
	const Expression& base_expression;
	const Substitution& substitution;
	vector<SubstitutedExpression> substituted_child;
	
	bool replaced(void) const
	{
		return base_expression.is_variable() && substitution.count((const VariableExpression&)base_expression);
	}
	
	const Expression& replacement(void) const
	{
		return *substitution.at((const VariableExpression&)base_expression);
	}

public:
	SubstitutedExpression(const Expression& e, const Substitution& s)
	: base_expression(e), substitution(s)
	{
		if(!replaced())
		{
			substituted_child.reserve(base_expression.size());
			for(const auto& child : base_expression)
				substituted_child.push_back(SubstitutedExpression((const Expression&)child, s));
		}
	}
	
	const Symbol& get_symbol(void) const
	{
		if(replaced())
			return replacement().get_symbol();
		else
			return base_expression.get_symbol();
	}
	
	size_t size(void) const
	{
		if(replaced())
			return replacement().size();
		else
			return base_expression.size();
	}
	
	const Notation& operator [] (size_t index) const
	{
		if(index > size())
			throw IndexError("", index, size(), this);
		
		if(replaced())
			return replacement()[index];
		else
			return substituted_child[index];
	}
	
	const VariableSet& capture_variables(void) const
	{
		if(replaced())
			return replacement().capture_variables();
		else
			return base_expression.capture_variables();
	}
	
	bool is_constant(void) const
	{
		if(replaced())
			return replacement().is_constant();
		else
			return base_expression.is_constant();
	}
	
	bool is_capture(void) const
	{
		if(replaced())
			return replacement().is_capture();
		else
			return base_expression.is_capture();
	}
	
	bool is_variable(void) const
	{
		if(replaced())
			return replacement().is_variable();
		else
			return base_expression.is_variable();
	}
};


inline uint64_t Notation::hash(uint64_t seed) const
{
	seed = get_symbol().hash(seed);
	for(const auto& child : *this)
		seed = child.hash(seed);
	return seed;
}


inline VariableSet Notation::free_variables(void) const
{
	VariableSet fv;
	
	for(const auto& child : *this)
		fv.merge(child.free_variables());
	
	for(const auto& var : capture_variables())
		fv.erase(var);
	
	return fv;
}


inline VariableSet VariableExpression::free_variables(void) const
{
	VariableSet fv;
	fv.insert(*this);
	return fv;
}


inline bool identical(const Notation& x, const Notation& y)
{
	if(&x == &y)
		return true;
	if(x.get_symbol() != y.get_symbol())
		return false;
	if(x.size() != y.size())
		return false;
	
	const size_t s = x.size();
	for(size_t i = 0; i < s; i++)
		if(!identical(x[i], y[i]))
			return false;
	
	return true;
}


inline uint64_t VariableHash::operator () (const VariableExpression& v) const
{
	return v.hash();
}

inline bool VariableCompare::operator () (const VariableExpression& u, const VariableExpression& v) const
{
	return identical(u, v);
}

const VariableSet empty_variable_set = VariableSet();

inline ExpressionPointer make_expression_pointer(const Expression& expression)
{
	return &expression;
}

inline SubstitutedExpression Expression::substitute(const Substitution& substitution) const
{
	return SubstitutedExpression(*this, substitution);
}

template<typename... Formulas>
constexpr auto ConnectiveSymbol::operator () (Formulas&&... f) const
{
	return CompoundFormula(*this, make_tuple<Formulas...>(forward<Formulas>(f)...));
}

template<typename... Expressions>
constexpr auto RelationSymbol::operator () (Expressions&&... e) const
{
	return AtomicFormula(*this, make_tuple<Expressions...>(forward<Expressions>(e)...));
}

template<typename... Expressions>
constexpr auto FunctionSymbol::operator () (Expressions&&... e) const
{
	return ConstantExpression(*this, make_tuple<Expressions...>(forward<Expressions>(e)...));
}

constexpr auto VariableSymbol::operator () (void) const
{
	return VariableExpression(*this);
}


size_t Notation::total_size(void) const
{
	size_t ts = 1;
	for(const auto& child : (*this))
		ts += child.total_size();
	ts += capture_variables().size();
	return ts;
}


constexpr auto True_Symbol = ConnectiveSymbol("⊤", true);
constexpr auto False_Symbol = ConnectiveSymbol("⊥", true);
constexpr auto Not = ConnectiveSymbol("¬", true);
constexpr auto And = ConnectiveSymbol("∧", true);
constexpr auto Or = ConnectiveSymbol("∨", true);
constexpr auto Xor = ConnectiveSymbol("⊻", true);
constexpr auto Equiv = ConnectiveSymbol("↔", true);
constexpr auto Impl = ConnectiveSymbol("→", true);
constexpr auto RImpl = ConnectiveSymbol("←", true);

constexpr auto NAnd = ConnectiveSymbol("!∧", true);
constexpr auto NOr = ConnectiveSymbol("!∨", true);
constexpr auto NXor = ConnectiveSymbol("!⊻", true);
constexpr auto NEquiv = ConnectiveSymbol("!↔", true);
constexpr auto NImpl = ConnectiveSymbol("!→", true);
constexpr auto NRImpl = ConnectiveSymbol("!←", true);


const auto True = True_Symbol();
const auto False = False_Symbol();


/*
template<typename Formula1, typename = enable_if_t<is_base_of_v<Formula, Formula1>>>
inline constexpr auto operator ~ (Formula1&& one)
{
	return Not(forward<Formula1>(one));
}

template<typename Formula1, typename Formula2, typename = enable_if_t<is_base_of_v<Formula, Formula1>>, typename = enable_if_t<is_base_of_v<Formula, Formula2>>>
inline constexpr auto operator & (Formula1&& one, Formula2&& two)
{
	return And(forward<Formula1>(one), forward<Formula2>(two));
}

template<typename Formula1, typename Formula2, typename = enable_if_t<is_base_of_v<Formula, Formula1>>, typename = enable_if_t<is_base_of_v<Formula, Formula2>>>
inline constexpr auto operator | (Formula1&& one, Formula2&& two)
{
	return Or(forward<Formula1>(one), forward<Formula2>(two));
}
*/


template<typename Formula1>
inline constexpr auto operator ~ (Formula1&& one)
{
	return Not(forward<Formula1>(one));
}

template<typename Formula1, typename Formula2>
inline constexpr auto operator & (Formula1&& one, Formula2&& two)
{
	return And(forward<Formula1>(one), forward<Formula2>(two));
}

template<typename Formula1, typename Formula2>
inline constexpr auto operator | (Formula1&& one, Formula2&& two)
{
	return Or(forward<Formula1>(one), forward<Formula2>(two));
}

template<typename Formula1, typename Formula2>
inline constexpr auto operator ^ (Formula1&& one, Formula2&& two)
{
	return Xor(forward<Formula1>(one), forward<Formula2>(two));
}

template<typename Formula1, typename Formula2, typename = enable_if_t<is_base_of_v<Formula, Formula1>>, typename = enable_if_t<is_base_of_v<Formula, Formula2>>>
inline constexpr auto operator % (Formula1&& one, Formula2&& two)
{
	return Equiv(forward<Formula1>(one), forward<Formula2>(two));
}

template<typename Formula1, typename Formula2, typename = enable_if_t<is_base_of_v<Formula, Formula1>>, typename = enable_if_t<is_base_of_v<Formula, Formula2>>>
inline constexpr auto operator >> (Formula1&& one, Formula2&& two)
{
	return Impl(forward<Formula1>(one), forward<Formula2>(two));
}

template<typename Formula1, typename Formula2, typename = enable_if_t<is_base_of_v<Formula, Formula1>>, typename = enable_if_t<is_base_of_v<Formula, Formula2>>>
inline constexpr auto operator << (Formula1&& one, Formula2&& two)
{
	return RImpl(forward<Formula1>(one), forward<Formula2>(two));
}


const auto Equal = RelationSymbol("=", true);
const auto Inequal = RelationSymbol("≠", true);

template<typename Expression1, typename Expression2, typename = enable_if_t<is_base_of_v<Expression, Expression1>>, typename = enable_if_t<is_base_of_v<Expression, Expression2>>>
inline constexpr auto operator == (Expression1&& one, Expression2&& two)
{
	return Equal(forward<Expression1>(one), forward<Expression2>(two));
}

template<typename Expression1, typename Expression2, typename = enable_if_t<is_base_of_v<Expression, Expression1>>, typename = enable_if_t<is_base_of_v<Expression, Expression2>>>
inline constexpr auto operator != (Expression1&& one, Expression2&& two)
{
	return Inequal(forward<Expression1>(one), forward<Expression2>(two));
}


Notation::operator bool (void) const
{
	if(get_symbol() == Equal)
	{
		if(size() != 2)
			throw RuntimeError("Invalid number of arguments.");
		return identical((*this)[0], (*this)[1]);
	}
	else if(get_symbol() == Inequal)
	{
		if(size() != 2)
			throw RuntimeError("Invalid number of arguments.");
		return !identical((*this)[0], (*this)[1]);
	}
	else
	{
		throw RuntimeError("Unsupported operator.");
	}
}


const auto Plus = FunctionSymbol("+", true);
const auto Minus = FunctionSymbol("-", true);

template<typename Expression1, typename Expression2, typename = enable_if_t<is_base_of_v<Expression, Expression1>>, typename = enable_if_t<is_base_of_v<Expression, Expression2>>>
inline constexpr auto operator + (Expression1&& one, Expression2&& two)
{
	return Plus(forward<Expression1>(one), forward<Expression2>(two));
}

template<typename Expression1, typename Expression2, typename = enable_if_t<is_base_of_v<Expression, Expression1>>, typename = enable_if_t<is_base_of_v<Expression, Expression2>>>
inline constexpr auto operator - (Expression1&& one, Expression2&& two)
{
	return Minus(forward<Expression1>(one), forward<Expression2>(two));
}


}; /* namespace Logical */


#ifdef DEBUG


#include <iostream>

using namespace Logical;
using std::cout;
using std::endl;


namespace LogicalNotationDebug
{

const auto f = FunctionSymbol("f", false);
const auto g = FunctionSymbol("g", false);
const auto a = FunctionSymbol("a", false)();
const auto b = FunctionSymbol("b", false)();
const auto x = VariableSymbol("x", true)();
const auto y = VariableSymbol("y", true)();
const auto z = VariableSymbol("z", true)();

const auto test_0 = ~True;
const auto test_1 = True & False & ~True;
const auto test_2 = a + b - a - b;
const auto test_3 = (a + b) == (a - b);
const auto test_4 = (a + a) != (b - b);
const auto test_5 = test_3 & True & ~ test_4;
const auto test_6 = x + y;
const auto test_7 = x + a + b;
const auto test_8 = y + b;

};


namespace Logical
{

inline void notation_test(void)
{
	using namespace LogicalNotationDebug;
	
	cout << test_0 << endl;
	cout << test_1 << endl;
	cout << test_2 << endl;
	cout << test_3 << endl;
	cout << test_4 << endl;
	cout << test_5 << endl;
	cout << test_6 << endl;
	cout << test_7 << endl;
	cout << test_8 << endl;
	
	logical_assert(a == a);
	logical_assert(f(a) == f(a));
	logical_assert(a != b);
}

}; /* namespace Logical */


#endif /* DEBUG */

#endif /* LOGICAL_NOTATION_HH */
