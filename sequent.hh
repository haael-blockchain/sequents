
#ifndef LOGICAL_SEQUENT_HH
#define LOGICAL_SEQUENT_HH

#include "collections.hh"
#include "errors.hh"
#include "notation.hh"
#include "logical.hh"
#include "unionfind.hh"
#include "unification.hh"


namespace Logical
{

using std::pair;


static inline float fabs(float x)
{
	if(x >= 0)
		return x;
	else
		return -x;
}


static inline Singleton<Formula> singleton(const Formula& formula)
{
	return Singleton<Formula>(formula);
}

static inline Shadow<Formula> shadow(const Formula& formula)
{
	return Shadow<Formula>(formula);
}

static inline Shadow<GeneralCompoundFormula> shadow(const GeneralCompoundFormula& formula)
{
	return Shadow<GeneralCompoundFormula>(formula);
}


class Sequent
{
private:
	class FormulaCompareCache;
	class ExpressionCompareCache;
	
	FormulaCompareCache* formulacomparecache;
	ExpressionCompareCache* expressioncomparecache;
	bool toplevel;
	const Unfold<Formula> left;
	const Unfold<Formula> right;
	
	template<typename LeftInitializer, typename RightInitializer>
	Sequent(LeftInitializer&& l, RightInitializer&& r, FormulaCompareCache* uf, ExpressionCompareCache* ue)
	 : left(forward<LeftInitializer>(l))
	 , right(forward<RightInitializer>(r))
	 , formulacomparecache(uf)
	 , expressioncomparecache(ue)
	 , toplevel(false)
	{
	}

protected:
	float guide_positive(const Formula& formula)
	{
		return formula.total_size();
	}
	
	float guide_negative(const Formula& formula)
	{
		return formula.total_size();
	}
	
	float guide_equal(const Formula& first, const Formula& second)
	{
		return (first.total_size() + second.total_size()) * (1.0f + fabs(float(first.total_size()) - float(second.total_size())));
	}
	
	float guide_equal(const Expression& first, const Expression& second)
	{
		return (first.size() + second.size()) * (1.0f + fabs(float(first.size()) - float(second.size())));
	}

private:
	template <typename LeftInitializer, typename RightInitializer>
	static bool sub_prove(LeftInitializer&& l, RightInitializer&& r, FormulaCompareCache* uf, ExpressionCompareCache* ue)
	{
		return Sequent(forward<LeftInitializer>(l), forward<RightInitializer>(r), uf, ue).prove();
	}
	
	//bool breakdown(const GeneralAtomicFormula& formula)
	//{
	//}
	
	bool breakdown(const GeneralCompoundFormula& formula)
	{
		//cerr << "breakdown: " << formula << endl;
		
		if(left.count(formula))
		{
			const auto singleton_formula = singleton(formula);
			const auto left_sans_formula = left - singleton_formula;
			
			logical_assert(left.count(formula));
			logical_assert(!left_sans_formula.count(formula));
			logical_assert(left_sans_formula.size() == left.size() - 1);
			logical_assert(Unfold<Formula>(left_sans_formula).size() == left_sans_formula.size());
			
			switch(formula.get_symbol())
			{
			case True_Symbol:
				return sub_prove(left_sans_formula, right, formulacomparecache, expressioncomparecache);
			
			case False_Symbol:
				return true;
			
			case Not:
				return sub_prove(left_sans_formula, right + singleton(formula[0]), formulacomparecache, expressioncomparecache);
			
			case RImpl:
				return shadow(formula).for_any([this, &left_sans_formula, &formula](const auto& subformula) {
					if(&subformula == &formula[0])
						return sub_prove(left_sans_formula + singleton(formula[0]), right, formulacomparecache, expressioncomparecache);
					else if(&subformula == &formula[1])
						return sub_prove(left_sans_formula, right + singleton(formula[1]), formulacomparecache, expressioncomparecache);
					else
						throw RuntimeError("None of the implication subformulas identical to the formula provided.");
				});
			
			case Impl:
				return shadow(formula).for_any([this, &left_sans_formula, &formula](const auto& subformula) {
					if(&subformula == &formula[1])
						return sub_prove(left_sans_formula + singleton(formula[1]), right, formulacomparecache, expressioncomparecache);
					else if(&subformula == &formula[0])
						return sub_prove(left_sans_formula, right + singleton(formula[0]), formulacomparecache, expressioncomparecache);
					else
						throw RuntimeError("None of the implication subformulas identical to the formula provided.");
				});
			
			case NRImpl:
				return sub_prove(left_sans_formula + singleton(formula[0]), right + singleton(formula[1]), formulacomparecache, expressioncomparecache);
			
			case NImpl:
				return sub_prove(left_sans_formula + singleton(formula[1]), right + singleton(formula[0]), formulacomparecache, expressioncomparecache);
			
			case And:
				return sub_prove(left_sans_formula + shadow(formula), right, formulacomparecache, expressioncomparecache);
			
			case Or:
				return shadow(formula)
				    .sort([this](const auto& f) { return guide_negative(f); })
				    .for_all([this, &left_sans_formula, &formula](const auto& subformula) { return sub_prove(left_sans_formula + singleton(subformula), right, formulacomparecache, expressioncomparecache); });
			
			case NOr:
				return sub_prove(left_sans_formula, right + shadow(formula), formulacomparecache, expressioncomparecache);
			
			case NAnd:
				return shadow(formula)
				    .sort([this](const auto& f) { return guide_positive(f); })
				    .for_all([this, &left_sans_formula, &formula](const auto& subformula) { return sub_prove(left_sans_formula, right + singleton(subformula), formulacomparecache, expressioncomparecache); });
			
			default:
				return false;
				// throw UnsupportedConnectiveError("Unsupported connective.", formula.get_symbol());
			}
			
			throw RuntimeError("Should not be here.");
		}
		
		if(right.count(formula))
		{
			const auto singleton_formula = singleton(formula);
			const auto right_sans_formula = right - singleton_formula;
			
			switch(formula.get_symbol())
			{
			case False_Symbol:
				return sub_prove(left, right_sans_formula, formulacomparecache, expressioncomparecache);
			
			case True_Symbol:
				return true;
			
			case Not:
				return sub_prove(left + singleton(formula[0]), right_sans_formula, formulacomparecache, expressioncomparecache);
			
			case NRImpl:
				return shadow(formula).for_any([this, &right_sans_formula, &formula](const auto& subformula) {
					if(&subformula == &formula[0])
						return sub_prove(right_sans_formula + singleton(formula[0]), right, formulacomparecache, expressioncomparecache);
					else if(&subformula == &formula[1])
						return sub_prove(right_sans_formula, right + singleton(formula[1]), formulacomparecache, expressioncomparecache);
					else
						throw RuntimeError("None of the implication subformulas identical to the formula provided.");
				});
			
			case NImpl:
				return shadow(formula).for_any([this, &right_sans_formula, &formula](const auto& subformula) {
					if(&subformula == &formula[1])
						return sub_prove(right_sans_formula + singleton(formula[1]), right, formulacomparecache, expressioncomparecache);
					else if(&subformula == &formula[0])
						return sub_prove(right_sans_formula, right + singleton(formula[0]), formulacomparecache, expressioncomparecache);
					else
						throw RuntimeError("None of the implication subformulas identical to the formula provided.");
				});
			
			case Impl:
				return sub_prove(left + singleton(formula[0]), right_sans_formula + singleton(formula[1]), formulacomparecache, expressioncomparecache);
			
			case RImpl:
				return sub_prove(left + singleton(formula[1]), right_sans_formula + singleton(formula[0]), formulacomparecache, expressioncomparecache);
			
			case Or:
				return sub_prove(left, right_sans_formula + shadow(formula), formulacomparecache, expressioncomparecache);
			
			case And:
				return shadow(formula)
				    .sort([this](const auto& f) { return guide_positive(f); })
				    .for_all([this, &right_sans_formula, &formula](
				                 const auto& subformula) { return sub_prove(left, right_sans_formula + singleton(subformula), formulacomparecache, expressioncomparecache); });
			
			case NAnd:
				return sub_prove(left + shadow(formula), right_sans_formula, formulacomparecache, expressioncomparecache);
			
			case NOr:
				return shadow(formula)
				    .sort([this](const auto& f) { return guide_negative(f); })
				    .for_all([this, &right_sans_formula, &formula](
				                 const auto& subformula) { return sub_prove(left + singleton(subformula), right_sans_formula, formulacomparecache, expressioncomparecache); });
			
			default:
				return false;
				// throw UnsupportedConnectiveError("Unsupported connective.", formula.get_symbol());
			}
			
			throw RuntimeError("Should not be here.");
		}
		
		throw RuntimeError("Formula not found on left nor right side of the sequent.");
	}
	
	bool breakdown(const Formula& formula)
	{
		return breakdown((const GeneralCompoundFormula&)formula);
		//TODO: atomic formula
	}
	
	bool equal(const Expression& first, const Expression& second)
	{
		if(expressioncomparecache)
			return expressioncomparecache->equal(first, second);
		else
			return expressions_equal(first, second);
	}
	
	bool expressions_equal(const Expression& first, const Expression& second)
	{
		try
		{
			unifier(first, second);
			return true;
		}
		catch(const UnificationError&)
		{
			return false;
		}
	}
	
	bool equal(const Formula& first, const Formula& second)
	{
		if(formulacomparecache)
			return formulacomparecache->equal(first, second);
		else
			return formulas_equal(first, second);
	}
	
	bool formulas_equal(const Formula& first, const Formula& second)
	{
		if(first.is_atomic() && second.is_atomic())
			return atomic_formulas_equal((const GeneralAtomicFormula&)first, (const GeneralAtomicFormula&)second);
		else if(first.is_compound() && second.is_compound())
			return compound_formulas_equal((const GeneralCompoundFormula&)first, (const GeneralCompoundFormula&)second);
		else
			return false;
	}
	
	bool compound_formulas_equal(const GeneralCompoundFormula& first, const GeneralCompoundFormula& second)
	{
		static const auto commutative_symbols = unordered_set<Symbol, SymbolHash>({And, Or, NAnd, NOr, Xor, NXor, Equiv, NEquiv});
		static const auto idempotent_symbols = unordered_set<Symbol, SymbolHash>({And, Or, NAnd, NOr});
		
		const auto& first_symbol = first.get_symbol();
		const auto& second_symbol = second.get_symbol();
		
		if(first_symbol != second_symbol) // root symbol mismatch
			return false;
		else if(first == second) // formulas syntactically identical
			return true;
		else if(first.is_atomic()) // atomic formula
		{
			if(first.size() != second.size())
				return false;
			
			return (shadow(first) % shadow(second))
			    .sort([this](const auto& p) { return -guide_equal(p.first, p.second); })
			    .for_all([this](const auto& p) { return equal(p.first, p.second); });
			
			throw RuntimeError("Not implemented."); // TODO
		}
		else if(first.is_quantifier()) // quantifier
		{
			//const CompoundFormula& first = static_cast<const CompoundFormula&>(first);
			//const CompoundFormula& second = static_cast<const CompoundFormula&>(second);
			
			throw RuntimeError("Not implemented."); // TODO
		}
		else if(commutative_symbols.count(first_symbol) && idempotent_symbols.count(first_symbol)) // commutative associative idempotent symbol, treat as set
		{
			//const CompoundFormula& first = static_cast<const CompoundFormula&>(first);
			//const CompoundFormula& second = static_cast<const CompoundFormula&>(second);
			
			const bool first_in_second = shadow(first).for_all([this, &second](const auto& sub1)
			{
				auto& parent = *this;
				return shadow(second)
				    .sort([&parent, &sub1](const auto& sub2) { return parent.guide_equal(sub1, sub2); })
				    .for_any([&parent, &sub1](const auto& sub2) { return parent.equal(sub1, sub2); });
			});
			
			const bool second_in_first = shadow(second).for_all([this, &first](const auto& sub2)
			{
				auto& parent = *this;
				return shadow(first)
				    .sort([&parent, &sub2](const auto& sub1) { return parent.guide_equal(sub2, sub1); })
				    .for_any([&parent, &sub2](const auto& sub1) { return parent.equal(sub2, sub1); });
			});
			
			return first_in_second && second_in_first;
		}
		else if(commutative_symbols.count(first_symbol) && !idempotent_symbols.count(first_symbol)) // commutative associative non-idempotent symbol, treat as multiset
		{
			//const CompoundFormula& first = static_cast<const CompoundFormula&>(first);
			//const CompoundFormula& second = static_cast<const CompoundFormula&>(second);
			
			if(first.size() != second.size())
				return false;
			
			auto shadow_first = shadow(first);
			auto shadow_second = shadow(second);
			
			return (shadow_first + shadow_second)
				.sort([](const auto& formula) { return formula.hash(); /* TODO: heuristic */ })
				.for_all([this, &shadow_first, &shadow_second](const auto& formula)
				{
					size_t one_count = 0, two_count = 0;
					
					for(const auto& one: shadow_first)
						if(equal(one, formula))
							one_count++;
					
					for(const auto& two: shadow_second)
						if(equal(two, formula))
							two_count++;
					
					return one_count == two_count;
				});
		}
		else if(first.is_compound())
		{
			//const CompoundFormula& first = static_cast<const CompoundFormula&>(first);
			//const CompoundFormula& second = static_cast<const CompoundFormula&>(second);
			
			if(first.size() != second.size())
				return false;
			
			return (shadow(first) % shadow(second))
			    .sort([this](const auto& p) { return -guide_equal(p.first, p.second); })
			    .for_all([this](const auto& p) { return equal(p.first, p.second); });
			
			throw RuntimeError("Not implemented."); // TODO
		}
		else
		{
			throw RuntimeError("Unsupported case.");
		}
	}

	class FormulaCompareCache : public CompareCache<Formula>
	{
	private:
		Sequent& sequent;

	protected:
		bool value_compare(const Formula& one, const Formula& two)
		{
			return sequent.formulas_equal(one, two);
		}

	public:
		FormulaCompareCache(Sequent& s)
		 : sequent(s)
		{
		}
	};
	
	class ExpressionCompareCache : public CompareCache<Expression>
	{
	private:
		Sequent& sequent;

	protected:
		bool value_compare(const Expression& one, const Expression& two)
		{
			return sequent.expressions_equal(one, two);
		}

	public:
		ExpressionCompareCache(Sequent& s)
		 : sequent(s)
		{
		}
	};

public:
	template<typename LeftInitializer, typename RightInitializer>
	Sequent(LeftInitializer&& l, RightInitializer&& r, bool usecache=true)
	 : left(forward<LeftInitializer>(l))
	 , right(forward<RightInitializer>(r))
	 , formulacomparecache(usecache ? new FormulaCompareCache(*this) : nullptr)
	 , expressioncomparecache(usecache ? new ExpressionCompareCache(*this) : nullptr)
	 , toplevel(true)
	{
	}
	
	~Sequent(void)
	{
		if(formulacomparecache && toplevel)
			delete formulacomparecache;
		if(expressioncomparecache && toplevel)
			delete expressioncomparecache;
	}
	
	bool prove(void)
	{
		//cerr << "prove " << (&left) << ", " << (&right) << endl;
		//cerr << left << " |- " << right << endl;
		
		return (left.size() == 0 && right.size() == 0)
		    || (left * right)
		           .sort([this](const pair<const Formula&, const Formula&>& p) { return guide_equal(p.first, p.second); })
		           .for_any([this](const pair<const Formula&, const Formula&>& p) { return equal(p.first, p.second); })
		    || (left + right)
		           .sort([this](const Formula& f) { return (left.count(f) ? guide_negative(f) : 0) + (right.count(f) ? guide_positive(f) : 0); })
		           .for_any([this](const Formula& f) { return breakdown(f); });
	}
};

class Formulas;

inline bool prove(const Formulas& l, const Formulas& r)
{
	return Sequent(l, r).prove();
}


} // namespace Logical

#ifdef DEBUG

namespace Logical
{

using std::remove_reference;


class Formulas
{
private:
	vector<unique_ptr<Formula>> child;

public:
	template<typename... FormulaT>
	Formulas(FormulaT&&... formulas)
	:child{make_unique<typename remove_reference<FormulaT>::type>(forward<FormulaT>(formulas))...}
	{
	}
	
	size_t size(void) const
	{
		return child.size();
	}
	
	const Formula& operator [] (size_t index) const
	{
		return *child[index];
	}
};



void sequent_test(void)
{
	try
	{
		const auto a = ConnectiveSymbol("a", false)();
		const auto b = ConnectiveSymbol("b", false)();
		const auto c = ConnectiveSymbol("c", false)();
		
		const auto ab = { a, b };
		logical_assert(Unfold<Formula>(ab).sort([](const Formula& f) -> float { return f.total_size(); }).for_any([&a, &b](const Formula& f) -> bool { return f == b; }));
		
        logical_assert(prove({}, {}), "Empty sequent should succeed.");
        logical_assert(prove({a}, {a}), "Sequent with the same symbol on both sides must succeed.");
        logical_assert(!prove({a}, {b}), "Sequent should fail.");
        logical_assert(prove({a}, {b, a}), "Sequent should succeed.");
        logical_assert(prove({a, b}, {a}), "Sequent should succeed.");
		
        logical_assert(!prove({}, {b}), "Sequent should fail.");
        logical_assert(!prove({}, {a}), "Sequent should fail.");
        logical_assert(!prove({Or(a, b)}, {b}), "Sequent should fail.");
        logical_assert(prove({And(a, b)}, {a}), "Sequent should succeed.");
        logical_assert(prove({}, {Or(a, Not(a))}), "Sequent should succeed.");
        logical_assert(prove({False}, {False}), "Sequent should succeed.");
        logical_assert(prove({}, {True}), "Sequent should succeed.");
        logical_assert(prove({a, Impl(a, b)}, {b}), "Sequent should succeed.");
        logical_assert(prove({Impl(a, b)}, {Or(Not(a), b)}), "Sequent should succeed.");
        logical_assert(prove({a}, {True}), "Sequent should succeed.");
        logical_assert(prove({a, b}, {a, b}), "Sequent should succeed.");
        logical_assert(prove({a, b}, {b, a}), "Sequent should succeed.");
        logical_assert(prove({a, b}, {And(a, b)}), "Sequent should succeed.");
        logical_assert(prove({Impl(a, b), Impl(Not(a), b)}, {b}), "Sequent should succeed.");
        logical_assert(prove({Not(a), a}, {}), "Sequent should succeed.");
        logical_assert(prove({a}, {a, b}), "Sequent should succeed.");
        logical_assert(prove({Impl(a, b), Impl(b, c)}, {Impl(a, c)}), "Sequent should succeed.");
        logical_assert(prove({Impl(a, b), Impl(a, c)}, {Impl(a, And(b, c))}), "Sequent should succeed.");
		
		logical_assert(!prove({a >> b}, {b >> a}), "Sequent of the form `a->b |- b->a` should fail.");
        logical_assert(prove({a | b, ~a}, {b}), "Sequent should succeed.");
		
		const auto x = VariableSymbol("x", false)();
		const auto y = VariableSymbol("y", false)();

		logical_assert(prove({Equal(x, x)}, {Equal(x, x)}));
		//logical_assert(!prove({Equal(x, x)}, {Equal(y, y)}));
	}
	catch(const UnsupportedConnectiveError& error)
	{
		cout << "UnsupportedConnectiveError.symbol = " << error.symbol << endl;
		throw error;
	}
}

} // namespace Logical

#endif // DEBUG

#endif // LOGICAL_SEQUENT_HH
