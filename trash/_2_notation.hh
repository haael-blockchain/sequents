
#ifndef LOGICAL_NOTATION_HH
#define LOGICAL_NOTATION_HH

#include "logical.hh"
#include "errors.hh"
#include <iostream>
#include <memory>
#include <string>
#include <string_view>
#include <vector>


namespace Logical
{

using std::cout;
using std::endl;
using std::forward;
using std::hex;
using std::move;
using std::ostream;
using std::string_view;
using std::vector;
using std::optional;
using std::addressof;
using std::nullptr_t;
using std::shared_ptr;
using std::make_shared;


class Symbol;
class ConnectiveSymbol;
class QuantifierSymbol;
class RelationSymbol;
class FunctionSymbol;
class VariableSymbol;

class Notation;
class NotationReference;
class Formula;
class BindingFormula;
class CompoundFormula;
class AtomicFormula;
class Expression;
class CaptureExpression;
class ConstantExpression;
class VariableExpression;


struct VariableHash;
struct VariablesIdentical;
class VariableSet;
class Substitution;



class Symbol
{
protected:
	enum class Type : uint8_t
	{
		CONNECTIVE, QUANTIFIER, RELATION, FUNCTION, CAPTURE, VARIABLE
	};

private:
	string_view value;
	Type type;

protected:
	constexpr Symbol(const string_view& s, const Type t)
	 : value(s)
	 , type(t)
	{
	}

public:
	bool is_connective(void) const { return type == Type::CONNECTIVE; }
	bool is_quantifier(void) const { return type == Type::QUANTIFIER; }
	bool is_relation(void) const { return type == Type::RELATION; }
	bool is_function(void) const { return type == Type::FUNCTION; }
	bool is_capture(void) const { return type == Type::CAPTURE; }
	bool is_variable(void) const { return type == Type::VARIABLE; }
	
	void operator = (const Symbol&) = delete;
	
	void print(ostream& out) const;
	
	const string_view& get_value(void) const
	{
		return value;
	}
	
	constexpr uint64_t hash(uint64_t seed = 0xa9236527) const
	{
		for(const char c : value)
			seed ^= ((seed << 6) + (seed >> (64 - 6)) + (c ^ (c >> 2)) + (((uint64_t)c) << 32) + 1);
		return seed;
	}
	
	constexpr operator uint64_t(void) const
	{
		return hash(0x94facb13);
	}
	
	bool operator == (const Symbol& that) const
	{
		return this == &that || (value == that.value && type == that.type);
	}
	
	bool operator != (const Symbol& that) const
	{
		return !((*this) == that);
	}
};


class ConnectiveSymbol : public Symbol
{
public:
	constexpr ConnectiveSymbol(const string_view& s)
	 : Symbol(s, Type::CONNECTIVE)
	{
	}
	
	template <typename... Args>
	CompoundFormula operator()(Args&&... args) const;
	//{
	//	return CompoundFormula(make_shared(*this), vector<NotationReference>(forward<Args>(args)...));
	//}
};











class Notation
{
public:
	bool is_formula(void) const { return is_binding() || is_compound() || is_atomic(); }
	bool is_binding(void) const { return get_symbol().is_quantifier(); }
	bool is_compound(void) const { return get_symbol().is_connective(); }
	bool is_atomic(void) const { return get_symbol().is_relation(); }
	bool is_expression(void) const { return is_capture() || is_constant() || is_variable(); }
	bool is_capture(void) const { return get_symbol().is_capture(); }
	bool is_constant(void) const { return get_symbol().is_function(); }
	bool is_variable(void) const { return get_symbol().is_variable(); }
	
	typedef Notation value_type;
	
	virtual const Symbol& get_symbol(void) const = 0;
	
	virtual size_t size(void) const = 0;
	
	virtual const Notation& operator[](size_t index) const = 0;
	
	auto begin(void) const
	{
		return Iterator<value_type>(*this, 0);
	}
	
	auto end(void) const
	{
		return Iterator<value_type>(*this, size());
	}
	
	bool operator == (const Notation& other) const;
	
	bool operator != (const Notation& other) const
	{
		return !((*this) == other);
	}
	
	uint64_t hash(uint64_t seed = 0xa759af0c) const;
	
	bool is_ground(void) const
	{
		if(is_variable())
			return false;
		
		for(const auto& child : (*this))
			if(!child.is_ground())
				return false;
		
		return true;
	}
	
	VariableSet free_variables(void) const;
	
	virtual NotationReference substitute(const Substitution& substitution) const = 0;
	
	virtual VariableSet capture_variables(void) const = 0;
	
	size_t total_size(void) const;
};


class NotationReference : public Notation
{
private:
	unique_ptr<Notation> notation;

public:
	constexpr NotationReference(const Notation& n) : notation(make_unique<Notation>(n)) {}
	constexpr NotationReference(Notation&& n) : notation(make_shared<Notation>(move(n))) {}
	constexpr NotationReference(const NotationReference& nr) : notation(nr.notation) {}
	constexpr NotationReference(NotationReference&& nr) : notation(move(nr.notation)) {}
	
	typedef Notation value_type;
	const Symbol& get_symbol(void) const { return notation->get_symbol(); }
	size_t size(void) const { return notation->size(); }
	const Notation& operator[](size_t index) const { return (*notation)[index]; }
	VariableSet capture_variables(void) const;
	//NotationReference substitute(const Substitution& substitution) const;
	NotationReference substitute(const Substitution& substitution) const { return notation->substitute(substitution); }
};


class Formula : public Notation
{
};


class CompoundFormula : public Formula
{
private:
	const ConnectiveSymbol& symbol;
	vector<NotationReference> formula;

public:
	constexpr CompoundFormula(const ConnectiveSymbol& s, vector<NotationReference>&& f) : symbol(s), formula(move(f))
	{
#ifdef DEBUG
		for(const auto& child : formula)
			logical_assert(child.is_formula());
#endif
	}
	
	typedef Formula value_type;
	
	const ConnectiveSymbol& get_symbol(void) const
	{
		return symbol;
	}
	
	size_t size(void) const
	{
		return formula.size();
	}
	
	const Formula& operator[] (size_t index) const
	{
		if(index < 0 || index > size())
			throw IndexError("Index out of range", index, size(), *this);
		return static_cast<const Formula&>(static_cast<const Notation&>(formula[index]));
	}
	
	VariableSet capture_variables(void) const;
	
	NotationReference substitute(const Substitution& substitution) const;
	/*{
		VariableSet vs;
		for(const auto& s : substitution)
			vs.insert(s.first);
		const VariableSet fv = free_variables();
		vs.erase(fv.begin(), fv.end());
		
		if(!vs.size()) return *this;
		
		vector<NotationReference> fs;
		fs.reserve(size());
		
		for(const Formula& f : *this)
			fs.insert(f.substitute(substitution));
		
		return CompoundFormula(symbol, move(fs));
	}*/
};


class BindingFormula : public Formula
{
private:
	shared_ptr<QuantifierSymbol> symbol;
	vector<VariableExpression> variable;
	vector<NotationReference> formula;

public:
	BindingFormula(shared_ptr<QuantifierSymbol>&& s, vector<VariableExpression>&& v, vector<NotationReference>&& f) : symbol(move(s)), variable(move(v)), formula(move(f))
	{
#ifdef DEBUG
		for(const auto& child : formula)
			assert(child.is_formula());
#endif
	}
	
	typedef Formula value_type;
	
	const Symbol& get_symbol(void) const
	{
		return *symbol;
	}
	
	size_t size(void) const
	{
		return formula.size();
	}
	
	const Formula& operator[] (size_t index) const
	{
		if(index < 0 || index > size())
			throw IndexError("Index out of range", index, size(), *this);
		return static_cast<const Formula&>(static_cast<const Notation&>(formula[index]));
	}
};


class AtomicFormula : public Formula
{
private:
	shared_ptr<RelationSymbol> symbol;
	vector<NotationReference> expression;

public:
	AtomicFormula(shared_ptr<RelationSymbol>&& s, vector<NotationReference>&& e) : symbol(move(s)), expression(move(e))
	{
#ifdef DEBUG
		for(const auto& child : expression)
			assert(child.is_expression());
#endif
	}
	
	typedef Expression value_type;
	
	const Symbol& get_symbol(void)
	{
		return *symbol;
	}
	
	size_t size(void) const
	{
		return expression.size();
	}
	
	const Expression& operator[] (size_t index) const
	{
		if(index < 0 || index > size())
			throw IndexError("Index out of range", index, size(), *this);
		return static_cast<const Expression&>(static_cast<const Notation&>(expression[index]));
	}
};


class Expression : public Notation
{
public:
	typedef Expression value_type;
};


class CaptureExpression : public Expression
{
private:
	shared_ptr<CaptureSymbol> symbol;
	vector<VariableExpression> variable;
	vector<NotationReference> expression;

public:
	CaptureExpression(shared_ptr<CaptureSymbol>&& s, vector<VariableExpression>&& v, vector<NotationReference>&& e) : symbol(move(s)), variable(move(v)), expression(move(e))
	{
#ifdef DEBUG
		for(const auto& child : expression)
			assert(child.is_expression());
#endif
	}
	
	const Symbol& get_symbol(void)
	{
		return *symbol;
	}
	
	size_t size(void) const
	{
		return expression.size();
	}
	
	const Expression& operator[] (size_t index) const
	{
		if(index < 0 || index > size())
			throw IndexError("Index out of range", index, size(), *this);
		return static_cast<const Expression&>(static_cast<const Notation&>(expression[index]));
	}
};


class ConstantExpression : public Expression
{
private:
	shared_ptr<FunctionSymbol> symbol;
	vector<NotationReference> expression;

public:
	ConstantExpression(shared_ptr<FunctionSymbol>&& s, vector<NotationReference>&& e) : symbol(move(s)), expression(move(e))
	{
#ifdef DEBUG
		for(const auto& child : expression)
			assert(child.is_expression());
#endif
	}
	
	const Symbol& get_symbol(void)
	{
		return *symbol;
	}
	
	size_t size(void) const
	{
		return expression.size();
	}
	
	const Expression& operator[] (size_t index) const
	{
		if(index < 0 || index > size())
			throw IndexError("Index out of range", index, size(), *this);
		return static_cast<const Expression&>(static_cast<const Notation&>(expression[index]));
	}
};


class VariableExpression : public Expression
{
private:
	shared_ptr<VariableSymbol> symbol;

public:
	VariableExpression(shared_ptr<VariableSymbol>&& s) : symbol(move(s)) {}
	
	const Symbol& get_symbol(void)
	{
		return *symbol;
	}
	
	size_t size(void) const
	{
		return 0;
	}
	
	const Expression& operator[] (size_t index) const
	{
		throw IndexError("Variables don't have members", index, size(), *this);
	}
};



/*
class Symbol
{
protected:
	enum class Type : uint8_t
	{
		CONNECTIVE, QUANTIFIER, RELATION, FUNCTION, VARIABLE
	};

private:
	string_view value;
	Type type;

protected:
	constexpr Symbol(const string_view& s, const Type t)
	 : value(s)
	 , type(t)
	{
	}

public:
	template <typename... Args>
	Notation operator()(Args&&... args) const = 0;
	
	void operator=(const Symbol&) = delete;
	
	void print(ostream& out) const;
	
	const string_view& get_value(void) const;
	
	constexpr uint64_t hash(uint64_t seed = 0xa9236527) const
	{
		for(char c : value)
			seed ^= ((seed << 6) + (seed >> (64 - 6)) + (c ^ (c >> 2)) + (((uint64_t)c) << 32) + 1);
		return seed;
	}
	
	constexpr operator uint64_t(void) const
	{
		return hash(0x94facb13);
	}
	
	bool operator==(const Symbol& that) const
	{
		return this == &that || (value == that->value && type == that->type);
	}
	
	bool operator!=(const Symbol& that) const
	{
		return !((*this) == that);
	}
	
	bool is_connective(void) const
	{
		return type == Type::CONNECTIVE;
	}
	
	bool is_function(void) const
	{
		return type == Type::FUNCTION;
	}
	
	bool is_variable(void) const
	{
		return type == Type::VARIABLE;
	}
	
	bool is_relation(void) const
	{
		return type == Type::RELATION;
	}
	
	bool is_quantifier(void) const
	{
		return type == Type::QUANTIFIER;
	}
};*/





class QuantifierSymbol : public Symbol
{
public:
	constexpr QuantifierSymbol(const string_view& s)
	 : Symbol(s, Type::QUANTIFIER)
	{
	}
	
	template <typename VariableCollection>
	class QuantifierApplication
	{
	private:
		VariableCollection variable;
		const Symbol& symbol;
	
	public:
		QuantifierApplication(VariableCollection&& v, const Symbol& s)
		 : variable(forward<VariableCollection>(v))
		 , symbol(s)
		{
		}
		
		template <typename... Args>
		BindingFormula operator()(Args&&... args)
		{
			return BindingFormula(make_shared(symbol), move(variable), vector<NotationReference>(forward<Args>(args)...));
		}
	};
	
	template <typename VariableCollection>
	auto operator[](VariableCollection&& vars) const
	{
		return QuantifierApplication<VariableCollection>(forward<VariableCollection>(vars), *this);
	}
	
	auto operator[](const initializer_list<VariableExpression>& vars) const
	{
		return QuantifierApplication<vector<VariableExpression>>(vector(vars), *this);
	}
};


class RelationSymbol : public Symbol
{
public:
	constexpr RelationSymbol(const string_view& s)
	 : Symbol(s, Type::RELATION)
	{
	}
	
	template <typename... Args>
	AtomicFormula operator()(Args&&... args) const
	{
		return AtomicFormula(make_shared(*this), vector<NotationReference>(forward<Args>(args)...));
	}
};


class CaptureSymbol : public Symbol
{
public:
	constexpr CaptureSymbol(const string_view& s)
	 : Symbol(s, Type::CAPTURE)
	{
	}
	
	template <typename VariableCollection>
	class CaptureApplication
	{
	private:
		VariableCollection variable;
		const Symbol& symbol;
	
	public:
		CaptureApplication(VariableCollection&& v, const Symbol& s)
		 : variable(forward<VariableCollection>(v))
		 , symbol(s)
		{
		}
		
		template <typename... Args>
		BindingFormula operator()(Args&&... args)
		{
			return CaptureExpression(make_shared(symbol), move(variable), vector<NotationReference>(forward<Args>(args)...));
		}
	};
	
	template <typename VariableCollection>
	auto operator[](VariableCollection&& vars) const
	{
		return CaptureApplication<VariableCollection>(forward<VariableCollection>(vars), *this);
	}
	
	auto operator[](const initializer_list<VariableExpression>& vars) const
	{
		return CaptureApplication<vector<VariableExpression>>(vector(vars), *this);
	}
};


class FunctionSymbol : public Symbol
{
public:
	constexpr FunctionSymbol(const string_view& s)
	 : Symbol(s, Type::FUNCTION)
	{
	}
	
	template <typename... Args>
	ConstantExpression operator()(Args&&... args) const
	{
		return ConstantExpression(make_shared(*this), vector<NotationReference>(forward<Args>(args)...));
	}
};


class VariableSymbol : public Symbol
{
public:
	constexpr VariableSymbol(const string_view& s)
	 : Symbol(s, Type::VARIABLE)
	{
	}
	
	template <typename... Args>
	VariableExpression operator()(Args&&... args) const
	{
		return VariableExpression(make_shared(*this), vector<NotationReference>(forward<Args>(args)...));
	}
};




constexpr auto Id = ConnectiveSymbol("");
constexpr auto Not = ConnectiveSymbol("~");

constexpr auto And = ConnectiveSymbol("∧");
constexpr auto Or = ConnectiveSymbol("∨");
constexpr auto NAnd = ConnectiveSymbol("⊼");
constexpr auto NOr = ConnectiveSymbol("⊽");

constexpr auto Xor = ConnectiveSymbol("⊻");
constexpr auto NXor = ConnectiveSymbol("⩝");
constexpr auto Equiv = ConnectiveSymbol("↔");
constexpr auto NEquiv = ConnectiveSymbol("↮");

constexpr auto Impl = ConnectiveSymbol("→");
constexpr auto NImpl = ConnectiveSymbol("↛");
constexpr auto RImpl = ConnectiveSymbol("←");
constexpr auto NRImpl = ConnectiveSymbol("↚");

constexpr auto ForAll = QuantifierSymbol("∀");
constexpr auto Exists = QuantifierSymbol("∃");
// constexpr auto Unique = QuantifierSymbol("∃!");

constexpr auto True = ConnectiveSymbol("⊤");
constexpr auto False = ConnectiveSymbol("⊥");

constexpr auto Ident = RelationSymbol("≡");
constexpr auto NIdent = RelationSymbol("≢");
constexpr auto Equal = RelationSymbol("=");
constexpr auto NEqual = RelationSymbol("≠");

constexpr auto Pred = RelationSymbol("≺");
constexpr auto Succ = RelationSymbol("≻");
constexpr auto EPred = RelationSymbol("≼");
constexpr auto ESucc = RelationSymbol("≽");
constexpr auto NPred = RelationSymbol("⊀");
constexpr auto NSucc = RelationSymbol("⊁");



inline constexpr CompoundFormula operator ~ (const Formula& one) { return Not(one); }
inline constexpr CompoundFormula operator & (const Formula& one, const Formula& two) { return And(one, two); }
inline constexpr CompoundFormula operator | (const Formula& one, const Formula& two) { return Or(one, two); }
inline constexpr CompoundFormula operator ^ (const Formula& one, const Formula& two) { return Xor(one, two); }
inline constexpr CompoundFormula operator % (const Formula& one, const Formula& two) { return Equiv(one, two); }
inline constexpr CompoundFormula operator << (const Formula& one, const Formula& two) { return RImpl(one, two); }
inline constexpr CompoundFormula operator >> (const Formula& one, const Formula& two) { return Impl(one, two); }




struct VariableHash
{
	uint64_t operator()(const VariableExpression& v) const;
};

struct VariablesIdentical
{
	bool operator()(const VariableExpression& a, const VariableExpression& b) const;
};

typedef unordered_set<VariableExpression, VariableHash, VariablesIdentical> VariableSet;
typedef unordered_map<VariableExpression, NotationReference, VariableHash, VariablesIdentical> Substitution;



inline VariableSet NotationReference::capture_variables(void) const
{
	return notation->capture_variables();
}


inline bool Notation::operator == (const Notation& other) const
{
	if(this == &other)
		return true;
	
	if(this->get_symbol() != other.get_symbol())
		return false;
	
	if(this->size() != other.size())
		return false;
	
	for(size_t i = 0; i < this->size(); i++)
		if((*this)[i] != other[i])
			return false;
	
	if(this->capture_variables() != other.capture_variables())
		return false;
	
	return true;
}


inline uint64_t Notation::hash(uint64_t seed) const
{
	uint64_t h = get_symbol().hash(seed);
	
	const uint64_t g = h + this->capture_variables().size();
	for(const auto& v : this->capture_variables())
		h ^= v.hash(g);
	
	h ^= this->size();
	
	for(const auto& c : (*this))
		h += c.hash(h) ^ (h >> 13);
	
	return h;
}


inline VariableSet Notation::free_variables(void) const
{
	VariableSet result;
	
	if(is_variable())
		result.insert(static_cast<const VariableExpression&>(*this));
	
	for(const auto& child : (*this))
		result.merge(child.free_variables());
	
	return result;
}

inline VariableSet Notation::capture_variables(void) const
{
	return VariableSet();
}

inline size_t Notation::total_size(void) const
{
	size_t s = this->capture_variables().size() + 1;
	for(const auto& child : (*this))
		s += child.total_size();
	return s;
}


uint64_t VariableHash::operator() (const VariableExpression& v) const
{
	return v.hash(0x1ac238a0);
}


bool VariablesIdentical::operator() (const VariableExpression& a, const VariableExpression& b) const
{
	return a == b;
}




/*struct ExpressionsIdentical
{
	constexpr ExpressionsIdentical(void)
	{
	}
	
	bool operator()(const Expression& a, const Expression& a) const
	{
		return a == b;
	}
};*/





/*

constexpr auto Everyone = ConnectiveSymbol("◻");
constexpr auto Someone = ConnectiveSymbol("◇");

constexpr auto WillAlways = ConnectiveSymbol("⟥");
constexpr auto WillOnce = ConnectiveSymbol("⟣");
constexpr auto WasAlways = ConnectiveSymbol("⟤");
constexpr auto WasOnce = ConnectiveSymbol("⟢");



constexpr auto Id = Symbol("⍳");

constexpr auto Assert = Symbol("⇶");
constexpr auto Dissert = Symbol("↯");

constexpr auto Lesser = Symbol("<");
constexpr auto NLesser = Symbol("≥");
constexpr auto Greater = Symbol(">");
constexpr auto NGreater = Symbol("≤");

constexpr auto Member = Symbol("∈");
constexpr auto NMember = Symbol("∉");
constexpr auto RMember = Symbol("∋");
constexpr auto NRMember = Symbol("∌");

constexpr auto PSubset = Symbol("⊂");
constexpr auto NPSubset = Symbol("⊄");
constexpr auto Subset = Symbol("⊆");
constexpr auto NSubset = Symbol("⊈");
constexpr auto RPSubset = Symbol("⊃");
constexpr auto NRPSubset = Symbol("⊅");
constexpr auto RSubset = Symbol("⊇");
constexpr auto NRSubset = Symbol("⊉");

constexpr auto Prove = Symbol("⊢");
constexpr auto NProve = Symbol("⊬");
constexpr auto Valid = Symbol("⊨");
constexpr auto Invalid = Symbol("⊭");
constexpr auto Force = Symbol("⊩");
constexpr auto NForce = Symbol("⊮");

*/

} // namespace Logical

#ifdef DEBUG

namespace Logical
{

void formula_valuetypes_test(void)
{
	// logical_assert(type_name<typename Difference<Shadow<Unfold<Formula> >, Singleton<Formula> >::value_type>() == "Logical::Formula");
	logical_assert(type_name<typename Shadow<CompoundFormula>::value_type>() == "Logical::Formula");
	// logical_assert(type_name<typename Concat<Difference<Shadow<Unfold<Formula> >, Singleton<Formula> >, Shadow<CompoundFormula> >::value_type>() ==
	// "Logical::Formula");
}

void formula_test(void)
{
	formula_valuetypes_test();

	const auto a = ConnectiveSymbol("a");
	const auto b = ConnectiveSymbol("b");

	logical_assert(a == a);
	logical_assert(a != b);
	logical_assert(b != a);
	logical_assert(b == b);

	logical_assert(a() == a());
	logical_assert(a() != b());
	logical_assert(b() != a());
	logical_assert(b() == b());

	logical_assert(a().size() == 0);
	logical_assert(a().total_size() == 1);

	logical_assert(Or(a(), b()) == Or(a(), b()));
	logical_assert((a() & b()) == (a() & b()));

	const auto x = VariableSymbol("x")();
	const auto y = VariableSymbol("y")();
	const auto x_prim = VariableSymbol("x")();

	logical_assert(Equal(x, y) == Equal(x, y));
	logical_assert(Equal(x, x) != Equal(y, y));

	const auto f1 = ForAll[x](Equal(x, x));
	const auto f2 = ForAll[y](Equal(y, y));
	const auto f1_prim = ForAll[x_prim](Equal(x, x_prim));

	logical_assert(f1 == f1_prim);
}

} // namespace Logical

#endif // DEBUG

#endif // LOGICAL_NOTATION_HH
