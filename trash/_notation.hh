
#ifndef LOGICAL_FORMULA_HH
#define LOGICAL_FORMULA_HH

#include "errors.hh"
#include "expression.hh"
#include "logical.hh"
#include <iostream>
#include <memory>
#include <string>
#include <string_view>
#include <vector>


namespace Logical
{

using std::cout;
using std::endl;
using std::forward;
using std::hex;
using std::move;
using std::ostream;
using std::string_view;
using std::vector;
using std::optional;
using std::addressof;
using std::nullptr_t;


class Symbol;


class Notation;
class NotationReference;
class Formula;
class BindingFormula;
class CompoundFormula;
class AtomicFormula;
class Expression;
class CaptureExpression;
class ConstantExpression;
class VariableExpression;




class Notation
{
private:
	const Symbol& symbol;

public:
	bool is_formula(void) const;
	bool is_binding(void) const;
	bool is_compound(void) const;
	bool is_atomic(void) const;
	bool is_expression(void) const;
	bool is_capture(void) const;
	bool is_constant(void) const;
	bool is_variable(void) const;
	
	const Symbol& get_symbol(void) const;
	
	typedef Notation value_type;
	
	virtual size_t size(void) const = 0;
	
	virtual const Notation& operator[](size_t index) const = 0;
	
	auto begin(void) const
	{
		return Iterator<value_type>(*this, 0);
	}
	
	auto end(void) const
	{
		return Iterator<value_type>(*this, size());
	}
	
	bool is_ground(void) const
	{
		if(is_variable())
			return false;
		
		for(const auto& child : (*this))
			if(!child.is_ground())
				return false;
		
		return true;
	}
	
	VariableSet free_variables(void) const
	{
		VariableSet result;
		
		if(is_variable())
			result.insert(*this);
		
		for(const auto& child : (*this))
			result.update(child.free_variables());
		
		return result;
	}
	
	virtual NotationReference substitute(const Substitution& substitution) const;
	
	virtual VariableSet capture_variables(void) const
	{
		return VariableSet();
	}
};


class NotationReference : public Notation
{
private:
	unique_ptr<Notation> notation;

public:
	template<typename NotationT>
	NotationReference(NotationT&& n)
	: notation(forward<NotationT>(n))
	{
	}
	
	bool is_formula(void) const { return notation->is_formula(); }
	bool is_binding(void) const { return notation->is_binding(); }
	bool is_compound(void) const { return notation->is_compound(); }
	bool is_atomic(void) const { return notation->is_atomic(); }
	bool is_expression(void) const { return notation->is_expression(); }
	bool is_capture(void) const { return notation->is_capture(); }
	bool is_constant(void) const { return notation->is_constant(); }
	bool is_variable(void) const { return notation->is_variable(); }
	
	typedef Notation value_type;
	size_t size(void) const { return notation->size(); }
	const Notation& operator[](size_t index) const { return (*notation)[index]; }
	auto begin(void) const { return notation->begin(); }
	auto end(void) const { return notation->end(); }
	
	bool is_ground(void) const { return notation->is_ground(); }
	VariableSet free_variables(void) const { return notation->free_variables(); }
	NotationReference substitute(const Substitution& substitution) const { return notation->substite(substitution); }
	VariableSet capture_variables(void) const { return notation->capture_variables(); }
};



class Formula : public Notation
{

};


class CompoundFormula : public Formula
{
private:
	vector<NotationReference> formula;

public:
	const Formula& operator[] (size_t index) const
	{
		return static_cast<const Formula&>(formula[index]);
	}
};


class BindingFormula : public Formula
{
private:
	vector<VariableExpression> variable;
	vector<NotationReference> formula;

public:
	const Formula& operator[] (size_t index) const
	{
		return static_cast<const Formula&>(formula[index]);
	}
};


class AtomicFormula : public Formula
{
private:
	vector<NotationReference> expression;

public:
	const Expression& operator[] (size_t index) const
	{
		return static_cast<const Expression&>(expression[index]);
	}
};


class Expression : public Notation
{

};


class BindingExpression : public Expression
{
private:
	vector<VariableExpression> variable;
	vector<NotationReference> expression;

public:
	const Expression& operator[] (size_t index) const
	{
		return static_cast<const Expression&>(expression[index]);
	}
};


class ConstantExpression : public Expression
{
private:
	vector<NotationReference> expression;

public:
	const Expression& operator[] (size_t index) const
	{
		return static_cast<const Expression&>(expression[index]);
	}
};


class VariableExpression : public Expression
{

};



	uint64_t hash(uint64_t seed = 0) const
	{
		seed ^= symbol.hash(seed);
		if(is_atomic())
		{
			for(const auto& e : expression)
				seed ^= e.hash(seed + 3);
		}
		else
		{
			for(const auto& f : formula)
				seed ^= f.hash(seed);
		}
		return seed;
	}

	bool operator==(const Formula& that) const
	{
		static const auto expressions_identical = ExpressionsIdentical();

#if defined(DEBUG) && !defined(__clang__)
		if(!this)
			throw RuntimeError("'this' pointer is null");
		if(!&that)
			throw RuntimeError("'&that' pointer is null");
#endif

		if(this == &that)
			return true;

		if(symbol != that.symbol)
			return false;

		if(is_atomic())
		{
			if(expression.size() != that.expression.size())
				return false;

			for(size_t i = 0; i < expression.size(); i++)
				if(!expressions_identical(expression[i], that.expression[i]))
					return false;
			return true;
		}
		else
			return formula == that.formula;
	}


	size_t total_size(void) const;

	size_t depth(void) const
	{
		if(is_atomic())
			return 1;

		size_t d = 0;
		for(const auto& f : formula)
		{
			const size_t nd = f.depth();
			if(nd > d)
				d = nd;
		}
		return d + 1;
	}

	// template<FormulaRF> Formula operator / (FormulaRF&& d) const;
	template <typename FormulaRF>
	Formula operator%(FormulaRF&&) const;

	template <typename FormulaRF>
	Formula operator<<(FormulaRF&&) const;

	template <typename FormulaRF>
	Formula operator>>(FormulaRF&&) const;
	
	Formula operator~(void) const;
	
	template <typename FormulaRF>
	Formula operator&(FormulaRF&&) &&;

	template <typename FormulaRF>
	Formula operator|(FormulaRF&&) const;

	template <typename FormulaRF>
	Formula operator^(FormulaRF&&) const;





class CompoundFormula : public Formula
{
public:
	typedef Formula value_type;

	const Formula& operator[](const size_t index) const
	{
		return Formula::operator[](index);
	}
	
	operator const Formula&(void) const&
	{
		return static_cast<const Formula&>(*this);
	}
};


class AtomicFormula : public Formula
{
public:
	typedef Expression value_type;

	const Expression& operator[](const size_t index) const
	{
		return Formula::operator[](index);
	}
	
	operator const Formula&(void) const&
	{
		return static_cast<const Formula&>(*this);
	}
};


inline ostream& operator<<(ostream& stream, const Formula& f)
{
	f.print(stream);
	return stream;
}

inline ostream& operator<<(ostream& stream, const Symbol& s)
{
	s.print(stream);
	return stream;
}

template <typename... Args>
inline Formula ConnectiveSymbol::operator()(Args&&... args) const
{
	return Formula(*this, vector<Formula>({forward<Args>(args)...}));
}

template <typename VariableT>
template <typename... Args>
inline Formula QuantifierSymbol::QuantifierApplication<VariableT>::operator()(Args&&... args)
{
	return Formula(symbol, vector<Formula>({forward<Args>(args)...}), forward<VariableT>(variable));
}

template <typename... Args>
inline Formula RelationSymbol::operator()(Args&&... args) const
{
	return Formula(*this, vector<ExpressionReference>({forward<Args>(args)...}));
}

inline constexpr uint64_t Symbol::hash(uint64_t seed) const
{
	seed += rel * 109 + quant * 113 + 37;
	for(char c : value)
		seed = (257 * seed + (unsigned char)c + 13) ^ (seed >> (64 - 8));
	return seed;
}

inline constexpr Symbol::operator uint64_t(void) const
{
	return hash(0x38a10a1c);
}

inline void Symbol::print(ostream& out) const
{
	out << value;
}

inline void Formula::print(ostream& out) const
{
#ifdef DEBUG
	logical_assert(is_valid_object(this));
#endif
	
	out << symbol;
	out << "(";
	bool first = true;
	for(auto& f : formula) // FIXME
	{
		if(first)
			first = false;
		else
			out << ",";
		out << f;
	}
	out << ")";
}

inline size_t Formula::total_size(void) const
{
#ifdef DEBUG
	{
		lock_guard<mutex> lg(active_objects_mutex);
		logical_assert(active_objects.count(this) == 1);
	}
/*{
	static mutex total_size_mutex;
	lock_guard<mutex> lg(total_size_mutex);
	cerr << hex << "total_size this=" << this << " symbol=" << &symbol << "/" << symbol << endl;
}*/
#endif

	size_t s = 1;
	if(is_atomic())
	{
		return expression.size();
	}
	else
	{
		for(const auto& f : formula)
			s += f.total_size();
	}
	return s;
}

constexpr auto Id = ConnectiveSymbol("");
constexpr auto Not = ConnectiveSymbol("~");

constexpr auto And = ConnectiveSymbol("∧");
constexpr auto Or = ConnectiveSymbol("∨");
constexpr auto NAnd = ConnectiveSymbol("⊼");
constexpr auto NOr = ConnectiveSymbol("⊽");

constexpr auto Xor = ConnectiveSymbol("⊻");
constexpr auto NXor = ConnectiveSymbol("⩝");
constexpr auto Equiv = ConnectiveSymbol("↔");
constexpr auto NEquiv = ConnectiveSymbol("↮");

constexpr auto Impl = ConnectiveSymbol("→");
constexpr auto NImpl = ConnectiveSymbol("↛");
constexpr auto RImpl = ConnectiveSymbol("←");
constexpr auto NRImpl = ConnectiveSymbol("↚");

constexpr auto ForAll = QuantifierSymbol("∀");
constexpr auto Exists = QuantifierSymbol("∃");
// constexpr auto Unique = QuantifierSymbol("∃!");

constexpr auto True = ConnectiveSymbol("⊤");
constexpr auto False = ConnectiveSymbol("⊥");

constexpr auto Ident = RelationSymbol("≡");
constexpr auto NIdent = RelationSymbol("≢");
constexpr auto Equal = RelationSymbol("=");
constexpr auto NEqual = RelationSymbol("≠");

constexpr auto Pred = RelationSymbol("≺");
constexpr auto Succ = RelationSymbol("≻");
constexpr auto EPred = RelationSymbol("≼");
constexpr auto ESucc = RelationSymbol("≽");
constexpr auto NPred = RelationSymbol("⊀");
constexpr auto NSucc = RelationSymbol("⊁");

template <typename FormulaRF>
inline Formula Formula::operator%(FormulaRF&& that) const
{
	return Equiv(*this, forward<FormulaRF>(that));
}

template <typename FormulaRF>
inline Formula Formula::operator<<(FormulaRF&& that) const
{
	return Impl(*this, forward<FormulaRF>(that));
}

template <typename FormulaRF>
inline Formula Formula::operator>>(FormulaRF&& that) const
{
	return RImpl(*this, forward<FormulaRF>(that));
}

inline Formula Formula::operator~(void) const
{
	return Not(*this);
}

template <typename FormulaRF>
inline Formula Formula::operator&(FormulaRF&& that) &&
{
	return And(move(*this), forward<FormulaRF>(that));
}

template <typename FormulaRF>
inline Formula Formula::operator|(FormulaRF&& that) const
{
	return Or(*this, forward<FormulaRF>(that));
}

template <typename FormulaRF>
inline Formula Formula::operator^(FormulaRF&& that) const
{
	return Xor(*this, forward<FormulaRF>(that));
}

/*

constexpr auto Everyone = ConnectiveSymbol("◻");
constexpr auto Someone = ConnectiveSymbol("◇");

constexpr auto WillAlways = ConnectiveSymbol("⟥");
constexpr auto WillOnce = ConnectiveSymbol("⟣");
constexpr auto WasAlways = ConnectiveSymbol("⟤");
constexpr auto WasOnce = ConnectiveSymbol("⟢");



constexpr auto Id = Symbol("⍳");

constexpr auto Assert = Symbol("⇶");
constexpr auto Dissert = Symbol("↯");

constexpr auto Lesser = Symbol("<");
constexpr auto NLesser = Symbol("≥");
constexpr auto Greater = Symbol(">");
constexpr auto NGreater = Symbol("≤");

constexpr auto Member = Symbol("∈");
constexpr auto NMember = Symbol("∉");
constexpr auto RMember = Symbol("∋");
constexpr auto NRMember = Symbol("∌");

constexpr auto PSubset = Symbol("⊂");
constexpr auto NPSubset = Symbol("⊄");
constexpr auto Subset = Symbol("⊆");
constexpr auto NSubset = Symbol("⊈");
constexpr auto RPSubset = Symbol("⊃");
constexpr auto NRPSubset = Symbol("⊅");
constexpr auto RSubset = Symbol("⊇");
constexpr auto NRSubset = Symbol("⊉");

constexpr auto Prove = Symbol("⊢");
constexpr auto NProve = Symbol("⊬");
constexpr auto Valid = Symbol("⊨");
constexpr auto Invalid = Symbol("⊭");
constexpr auto Force = Symbol("⊩");
constexpr auto NForce = Symbol("⊮");

*/

} // namespace Logical

#ifdef DEBUG

namespace Logical
{

void formula_valuetypes_test(void)
{
	// logical_assert(type_name<typename Difference<Shadow<Unfold<Formula> >, Singleton<Formula> >::value_type>() == "Logical::Formula");
	logical_assert(type_name<typename Shadow<CompoundFormula>::value_type>() == "Logical::Formula");
	// logical_assert(type_name<typename Concat<Difference<Shadow<Unfold<Formula> >, Singleton<Formula> >, Shadow<CompoundFormula> >::value_type>() ==
	// "Logical::Formula");
}

void formula_test(void)
{
	formula_valuetypes_test();

	const auto a = ConnectiveSymbol("a");
	const auto b = ConnectiveSymbol("b");

	logical_assert(a == a);
	logical_assert(a != b);
	logical_assert(b != a);
	logical_assert(b == b);

	logical_assert(a() == a());
	logical_assert(a() != b());
	logical_assert(b() != a());
	logical_assert(b() == b());

	logical_assert(a().size() == 0);
	logical_assert(a().total_size() == 1);

	logical_assert(Or(a(), b()) == Or(a(), b()));
	logical_assert((a() & b()) == (a() & b()));

	const auto x = Variable("x");
	const auto y = Variable("y");
	const auto x_prim = Variable("x");

	logical_assert(Equal(x, y) == Equal(x, y));
	logical_assert(Equal(x, x) != Equal(y, y));

	const auto f1 = ForAll[x](Equal(x, x));
	const auto f2 = ForAll[y](Equal(y, y));
	const auto f1_prim = ForAll[x_prim](Equal(x, x_prim));

	logical_assert(f1 == f1_prim);
}

} // namespace Logical

#endif // DEBUG

#endif // LOGICAL_FORMULA_HH
