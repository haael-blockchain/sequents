
#ifndef LOGICAL_LOGICAL_HH
#define LOGICAL_LOGICAL_HH

namespace Logical
{

template <typename Collection> class Iterator;
template <typename Item> class Empty;
template <typename Item> class Singleton;
template <typename Collection> class Shadow;
template <typename Collection1, typename Collection2> class Concat;
template <typename Collection1, typename Collection2> class Difference;
template <typename Collection1, typename Collection2> class Cartesian;
template <typename Collection1, typename Collection2> class Zip;
template <typename Collection> class Reorder;
template <typename Collection> class Parallel;

class UnionFind;
class Partition;

class Unifier;

class Symbol;
class ConnectiveSymbol;
class QuantifierSymbol;
class RelationSymbol;
class FunctionSymbol;
class CaptureSymbol;
class VariableSymbol;

class Notation;
class Formula;
template<typename> class CompoundFormula;
template<typename> class BindingFormula;
template<typename> class AtomicFormula;
class Expression;
template<typename> class ConstantExpression;
template<typename> class CaptureExpression;
class VariableExpression;
class SubstitutedExpression;

class Sequent;

} // namespace Logical

#endif // LOGICAL_LOGICAL_HH
